#This file is part product_barcode module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    image = fields.Binary('Image')
    image_icon = fields.Function(fields.Boolean('Image'), 'get_image_icon')

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()

    def get_image_icon(self, name=None):
        if self.image:
            return True
        return False

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['image'] = None
        return super(Product, cls).copy(records, default=default)


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'
    image = fields.Binary('Image')
    images = fields.One2Many('product.category.image', 'category', 'Images')
    description = fields.Char('Description')

    @classmethod
    def __setup__(cls):
        super(Category, cls).__setup__()


class CategoryImage(ModelSQL, ModelView):
    'Category Image'
    __name__ = 'product.category.image'
    category = fields.Many2One('product.category',
        'Product Catgegory', select=True, required=True)
    image = fields.Char('Image Link', required=True)
